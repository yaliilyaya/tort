<?php

class SiteController extends Controller
{
    /**  All layouts use css/styles.css from html5 boilerplate.
                  All less files are compiled into the css folder(you can define the output folder in WinLess).
         */

        /**
         *      Semantic,responsive  layout with Semantic Grid
         *       http://semantic.gs/
         *       This layout needs  less/sg/layout.less ,less/sg/responsive_sg.less and less/bs/bootstrap.less
         *        Also less/bs/responsive.less if you use the  bootstrap navbar for navigation
         *       Bootstrap is still used for non-layout design,so you can exclude layout related code in bootstrap.less.
        */
       // public $layout='//layouts/grid_sg';
    public $layout = '//layouts/main';


        /**
    	 *This layout is semantic with Bootstrap,it uses  layout mixins like .makeRow() and .makeColumn();
         * It uses    less/sem_layout.less and bootstrap.less(with all layout code optionally excluded),
         * The downside is that you have to code a custom responsive less or css file.The default bootstrap responsive.less
         *  file will not work,since it produces the non-semantic classes .span* which are not used in this semantic layout.
         *
    	 */
         //   public $layout='//layouts/grid_bs_sem';

    public function actions()
   	{
   		return array(
   			// captcha action renders the CAPTCHA image displayed on the contact page
   			'captcha'=>array(
   				'class'=>'CCaptchaAction',
   				'backColor'=>0xFFFFFF,
   			),
   			// page action renders "static" pages stored under 'protected/views/site/pages'
   			// They can be accessed via: index.php?r=site/page&view=FileName
   			'page'=>array(
   				'class'=>'CViewAction',
   			),
   		);
   	}


	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        $this->render('index');
	}



    /**
   	 * This is the action to handle external exceptions.
   	 */
   	public function actionError()
   	{
   	    if($error=Yii::app()->errorHandler->error)
   	    {
   	    	if(Yii::app()->request->isAjaxRequest)
   	    		echo $error['message'];
   	    	else
   	        	$this->render('error', $error);
   	    }
   	}





}
