<?php

class TotalListenersController extends Controller
{
	public function actionJson()
	{
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        echo json_encode($this->getData());
	}
    public function actionXml()
    {
        prAR(__FILE__);
    }

    public function getData() {
        $listeners = Yii::app()->IceCast->data['listeners'];
        return array('listeners' => floor($listeners * 2.3));
    }
    // Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}