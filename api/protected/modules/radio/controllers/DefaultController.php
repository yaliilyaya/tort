<?php

/**
 * Class DefaultController
 * Контроллер отвечает за вывод информации о канале по запросу пользователя
 * Подробнее в инструкции по использованию
 * @link http://tort.fm/content/tortfm-api
 */
class DefaultController extends Controller
{
    /**
     * @var array Список доступных типов для возврата информации
     */
    protected $type_list = ['xml', 'json'];

    /**
     * Возвращает количество пользователей канала
     * @param $channel
     * @param $type
     */
    public function actionListeners($channel, $type)
	{
        $rs = null;
        if ($this->isChannel($channel)) {
            $channel_data = Yii::app()->IceCast->getChannel($channel);
            $rs = ['listeners' => floor($channel_data['listeners'] * 2.3)];
        }
        $this->returnType($type, $rs);
	}

    /**
     * Возвращает список последних треков проигрываемых каналом
     * @param $channel
     * @param int $count колличество возвращаемых треков
     * @param $type
     */
    public function actionHistory($channel, $count, $type)
    {
        if ($this->isChannel($channel)) {
            $history_list = Yii::app()->IceCast->getHistory($channel, $count);
            foreach ($history_list as &$history) {
                $history['time'] = strtotime($history['date']);
                unset($history['date']);
            }
        }
        $this->returnType($type, $history_list);
    }

    /**
     * Возвращает текущую песню на канале
     * @param $channel
     * @param $type
     */
    public function actionSong($channel, $type)
    {
        $rs = null;
        if ($this->isChannel($channel)) {

            $channel_data = Yii::app()->IceCast->getChannel($channel);
            $rs = [['song' => $channel_data['title']]];
        }
        $this->returnType($type, $rs);
    }

    /**
     * Возврат пользователю запрашиваемой информации в заданном виде
     * @param $type
     * @param $data
     */
    protected function returnType($type, $data) {
        if (!in_array($type, $this->type_list)) {
            return;
        }
        $type = "return".ucfirst($type);
        $this->$type($data);
    }

    /**
     * Возврат информации в Json виде
     * @param $data
     */
    protected function returnJson($data) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    /**
     * Возврат информации в Xml виде
     * TODO:: реализовать вывод в XML
     * @param $data
     */
    protected function returnXml($data) {
        header('Access-Control-Allow-Origin: *');
        header('Content-Type: application/xml');
        $xml = new SimpleXMLElement('<root/>');
        print $xml->asXML();
    }

    /**
     * Проверяет валидность канала
     * @param $channel
     * @return bool
     */
    protected function isChannel($channel) {
        return Yii::app()->IceCast->isChannel($channel);
    }


}