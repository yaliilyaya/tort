<?php

/**
 * Created by PhpStorm.
 * User: yali
 * Date: 18.09.15
 * Time: 2:56
 */
class DbCommand extends CDbCommand
{
    public function insert($table, $columns)
    {
        return parent::insert($table, $columns);
        if (isset($columns[0]) && is_array($columns[0])) {

        }
        $columns_list = $columns;
        $params=array();
        $names=array();
        $placeholders=array();
        $inset_name = true;

        foreach ($columns_list as $key => $columns) {
            foreach($columns as $name => $value)
            {
                if ($inset_name) {
                    $names[]=$this->getConnection()->quoteColumnName($name);
                }
                if($value instanceof CDbExpression)
                {
                    $placeholders[] = $value->expression;
                    foreach($value->params as $n => $v)
                        $params[$n] = $v;
                }
                else
                {
                    $placeholders[] = ':' . $name;
                    $params[':' . $name] = $value;
                }
            }
            $inset_name = false;
        }
        $sql='INSERT INTO ' . $this->_connection->quoteTableName($table)
            . ' (' . implode(', ',$names) . ') VALUES ('
            . implode(', ', $placeholders) . ')';
        return $this->setText($sql)->execute($params);
    }
}