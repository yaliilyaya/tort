<?php

/**
 * Class IceCastApi
 * Класс API для общения с IceCast2
 * Осуществляет быстрый доступ к нужной информации с сервера IceCast
 * Обрабатывает как сам запрос с сервера так и лог файл плей листа.
 * @property array $data
 */
class IceCastApi extends CComponent
{
    /** Таблица для хранения истории проигрывания треков по всем каналам */
    const TABLE_PLAYLIST = "playlist";
    /** Конфигурация для полключения к IceCast2 */
    public $hostname = "";
    public $port = 0;
    public $username = "";
    public $password = "";
    public $channel = [];
    public $alias = [];
    /** Путь до лог файла плейлиста */
    public $file_log = "";

    protected $key_radio_list = [
        'listeners', 'channel_list'
    ];
    protected $key_channel_list = [
        'listeners', 'title'
    ];

    protected $_data = null;

    /**
     * При обращении с компоненту подгружаем всю информацию.
     * Также проверяет новые изменения в файле плей листа, и если они есть сохраняет их в БД
     * Кеш по умолчанию работает 15с
     */
    public function init() {
        $this->channel = array_merge($this->channel, array_keys($this->alias));
        $this->_data = Yii::app()->cache->get("dataicecast");
        if (empty($this->_data)) {
            $this->_data = $this->getInfo();
            $this->checkHistory();
            Yii::app()->cache->set("dataicecast", $this->_data, 15);
        }
    }

    /**
     * @return array
     */
    public function getData() {
        return $this->_data;
    }

    /**
     * Возвращает информацию определённого канала
     * Есть обработка аллиасов(сокращений названий) каналов
     * @param $channel
     * @return null
     */
    public function getChannel($channel) {
        if (isset($this->alias[$channel])) {
            return $this->getChannel($this->alias[$channel]);
        }
        else if (isset($this->data['channel_list'][$channel])) {
            return $this->data['channel_list'][$channel];
        }
        return null;
    }

    /**
     * Проверяет валидность канала
     * @param $channel
     * @return bool
     */
    public function isChannel($channel) {
        return in_array($channel, $this->channel);
    }

    /**
     * Запрашивает информацию с сервера IceCast2, обрабатывает её и возвращает в виде массива
     * @return array
     */
    protected function getInfo() {
        $xml = $this->getInfoXml();

        $data = (array)simplexml_load_string($xml);
        $channel_list = [];
        if (isset($data['source']) && is_array($data['source'])) {
            foreach ($data['source'] as $channel) {
                $channel = (array)$channel;
                $type = str_replace(array("/"), "", $channel['@attributes']['mount']);
                $channel = array_intersect_key($channel, array_flip($this->key_channel_list));
                $channel_list[$type] = $channel;
            }
        }
        $data['channel_list'] = $channel_list;
        return array_intersect_key($data, array_flip($this->key_radio_list));
    }

    /**
     * Получает информацию с сервера IceCast2 в Xml
     * @return mixed
     */
    protected function getInfoXml() {
        $process = curl_init($this->hostname.':'.$this->port.'/admin/stats');
        curl_setopt($process, CURLOPT_USERPWD, $this->username . ":" . $this->password);
        curl_setopt($process, CURLOPT_TIMEOUT, 5);
        curl_setopt($process, CURLOPT_RETURNTRANSFER, TRUE);

        return curl_exec($process);
    }

    /**
     * Возвращает историю проигрываемых треков определённого канала
     * @param null $channel
     * @param int $count
     * @return array|CDbDataReader
     */
    public function getHistory($channel = null, $count = 20) {
        $query = Yii::app()->db->createCommand()
            ->from(self::TABLE_PLAYLIST)
            ->order("date DESC")
            ->limit($count);
        if ($channel != null) {
            if (isset($this->alias[$channel])) {
                $channel = $this->alias[$channel];
            }
            $query->where("channel=:channel", ['channel' => $channel]);
        }
        return $query->queryAll();
    }

    /**
     * Проверяет присутствие изменений в лог файле плей листа
     * если изменения присутствуют то сохраняет их в БД
     */
    protected function checkHistory() {
        $log = array_reverse($this->getLog());
        $history_list = $this->getHistory(null, 1);
        if (!count($history_list)) {
            $this->insertDbLog(array_reverse($log));
            return;
        }
        $history_row = $history_list[0];
        $history_row['time'] = strtotime($history_row['date']);
        $result = [];

        foreach ($log as $row) {
            if (
                $row['channel'] == $history_row['channel']
                && $row['time'] == $history_row['time']
            ) {
                break;
            }
            $result[] = $row;
        }
        if(count($result)) {
            $this->insertDbLog(array_reverse($result));
        }
    }

    /**
     * Возвращает строки из лог-файла плей листа в виде массива
     * @param int $count
     * @return array
     */
    protected function getLog($count = 5) {
        $count++;

        $row_list = [];
        $offset = -1;
        $read = '';
        $fp = @fopen($this->file_log, "r");
        while( $count && fseek($fp, $offset, SEEK_END) >= 0 ) {
            $c = fgetc($fp);
            if($c == "\n" || $c == "\r"){
                $count--;
                $row_list[] = $this->parseRow(strrev(rtrim($read,"\n\r")));
                $read = "";
            }
            $read .= $c;
            $offset--;
        }
        fclose ($fp);
        return array_reverse(array_filter($row_list));
    }

    /**
     * Обрабатывает строку из лог-файла плей листа
     * @param $row
     * @return array|null
     */
    protected function parseRow($row) {
        $row = trim($row);
        if (empty($row)) {
            return null;
        }
        $temp_data = explode("|", $row);
        $data_song = $this->parseSong($temp_data[3]);
        $data = [
            'time' => strtotime($temp_data[0]),
            'channel' => str_replace("/", "", $temp_data[1]),
            'song_id' => intval($temp_data[2]),
        ];
        return array_merge($data, $data_song);
    }

    /**
     * Обрабатывает название песни из строки в лог-файле плей листа
     * @param $song
     * @return array
     */
    protected function parseSong($song) {
        $tmp = explode("-",htmlspecialchars($song)); // exploding to artist and title
        return [
            'song' => $song,
            'title' => trim(arrVal($tmp, 1)),
            'artist' => trim(arrVal($tmp, 0))
        ];

    }

    /**
     * сохраняет весь лог-файл плей листа в БД
     */
    protected function createDBLog() {
        $row_list = file($this->file_log);
        Yii::app()->db->createCommand()->truncateTable(self::TABLE_PLAYLIST);
        foreach ($row_list as &$row) {
            $row = $this->parseRow($row);
        }
        $this->insertDbLog($row_list);

    }

    /**
     * Сохраняет список треков в БД
     * //TODO:: create multi-insert
     * @param $row_list
     */
    protected function insertDbLog($row_list) {
        foreach ($row_list as &$row) {
            $row['date'] = date("Y.m.d H:i:s", $row['time']);
            unset($row['time']);
            Yii::app()->db->createCommand()
                ->insert(self::TABLE_PLAYLIST, $row);
        }
    }


}