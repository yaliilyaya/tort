<?php

/**
 * Created by PhpStorm.
 * User: yali
 * Date: 18.09.15
 * Time: 2:55
 */
class DbConnection extends CDbConnection
{
    public function createCommand($query=null)
    {
        $this->setActive(true);
        return new DbCommand($this, $query);
    }
}