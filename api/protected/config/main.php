<?php
function prAR($text = "") {
    echo "<pre>".print_r($text, 1)."</pre>";
}
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__)."/..",
	'name'=>'My Web Application',

	// preloading 'log' component
	'preload'=>array(
        'log',
 //   'bootstrap', // preload the bootstrap component
     ),



	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
        'application.ext.controller.*',
        'application.ext.runactions.controllers.RunActionsController',
        'application.ext.runactions.components.ERunActions'
	),
		'aliases'=>array(
		'bootstrap'=>'webroot.protected.extensions.bootstrap'
    ),
	'modules'=>array(
		// uncomment the following to enable the Gii tool
        'radio',
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'1',
           'generatorPaths'=>array(
        //    'bootstrap.gii', // since 0.9.1
            ),
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
        "api"
	),

	// application components
	'components'=>array(
        'IceCast' => [
            'class' => "application.components.IceCastApi",
            'hostname' => "radio.tort.fm",
            'port' => 80,
            'username' => "tort",
            'password' => "TortNEkeks2014",
            'channel' => ['tort.fm', 'trance', 'house'],
            'alias' => [
                'tort.fm' => 'tort.fm.nonstop',
                'trance' => 'trance.nonstop',
                'house' => 'house.nonstop',
            ],
            'file_log' => "/var/log/icecast2/playlist.log"
        ],
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

        /*by default we don't use the  extension,we use the original bootstrap js and css files,(with less).
         'bootstrap'=>array(
        'class'=>'ext.bootstrap.components.Bootstrap',
                ),*/
    
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
            'showScriptName'=>false,
			'rules'=>array(
                /* other @see http://www.yiiframework.com/doc/guide/1.1/en/topics.url */
                'radio/<channel:[\w\.]+>/listeners/<type:\w+>' => "radio/default/listeners",
                'radio/<channel:[\w\.]+>/history/<count:\d+>/<type:\w+>' => "radio/default/history",
                'radio/<channel:[\w\.]+>/song/<type:\w+>' => "radio/default/song",

                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
                'site/page/<view:\w+>' => 'site/page/',
			),
		),

		/*'db'=>array(
					'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
				),*/
		// uncomment the following to use a MySQL database

        'db'=> array(    //LOCALHOST
            'connectionString' => 'mysql:host=localhost;dbname=new_tort',
            'emulatePrepare' => true,
            'username' => 'tort',
            'password' => 'kPMoPT5CN8',
            'charset' => 'utf8'
        ),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
        'clientScript'=>array(
            'class' => 'CClientScript',
                    'scriptMap' => array(
                            'jquery.js'=>false,
                        'jquery.min.js'=>false
                    ),
            'coreScriptPosition' => CClientScript::POS_END,

        ),
        'cache'=>array(
            'class'=>'CMemCache',
            'serializer' => false,
            'servers'=>array(
               array('host'=>'localhost', 'port'=>11211, 'weight'=>60),
            ),
        ),


	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);