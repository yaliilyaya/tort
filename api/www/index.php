<?php

defined('LOCAL_DOMAIN') or define('LOCAL_DOMAIN','public.tort');
define('CURRENT_ACTIVE_DOMAIN', $_SERVER['HTTP_HOST']);
defined('APP_DEPLOYED') or define('APP_DEPLOYED',!(CURRENT_ACTIVE_DOMAIN == LOCAL_DOMAIN));
defined('DS') or define('DS',DIRECTORY_SEPARATOR);
define("DIR_ROOT", dirname(__FILE__));
//Local Framework Path
$yii = dirname(__FILE__).DS.'..'.DS.'..'.DS.'framework'.DS .'yii.php';

$config = dirname(__FILE__).'/../protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
$yii = Yii::createWebApplication($config)->run();
